package com.otaboroid.familytree;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.otaboroid.familytree.service.QuizActivity;
import com.otaboroid.familytree.service.QuizWrapper;
import com.otaboroid.familytree.service.SessionManagement;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends QuizActivity {

    // Session Manager Class
    SessionManagement session;
    public static final String TAG ="QZ_01" ;

    private String[] parsedObject= new String[4] ;
    //String[] jsonObject = new String[4];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Session class instance
        session = new SessionManagement(getApplicationContext());

        startAnimating();
    }

    /**
     * Helper method to start the animation on the splash screen
     */
    private void startAnimating() {
        // Fade in top title
        // Load animations for all views within the TableLayout
        Animation spinin = AnimationUtils.loadAnimation(this, R.anim.custom_anim);
        ImageView logo3 = (ImageView) findViewById(R.id.spinning_image) ;
        logo3.startAnimation(spinin);

        // Fade in bottom title after a built-in delay.
        TextView logo2 = (TextView) findViewById(R.id.TextViewBottomVersion);
        Animation fade2 = AnimationUtils.loadAnimation(this, R.anim.fade_in2);
        logo2.startAnimation(fade2);
        // Transition to Main Menu when bottom title finishes animating
        fade2.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                // The animation has ended, transition to the Main Menu screen

                /**
                 * Call this function whenever you want to check user login
                 * This will redirect user to LoginActivity is he is not
                 * logged in
                 * */

                AsyncJsonObject asyncObject = new AsyncJsonObject();
                asyncObject.execute("");

            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        });

        /*
        LayoutAnimationController controller = new LayoutAnimationController(spinin);
        TableLayout table = (TableLayout) findViewById(R.id.TableLayout01);
        for (int i = 0; i < table.getChildCount(); i++) {
            TableRow row = (TableRow) table.getChildAt(i);
            row.setLayoutAnimation(controller);
        }
        */
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Start animating at the beginning so we get the full splash screen experience
        startAnimating();
    }

    private class AsyncJsonObject extends AsyncTask<String, Void, String> {

        private ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {

            Log.v(TAG, "Ready to download");
            HttpClient httpClient = new DefaultHttpClient(new BasicHttpParams());
            HttpPost httpPost = new HttpPost("http://akdigitzed.gear.host/naija_quiz/index02.php");
            String jsonResult = "";

            try {
                HttpResponse response = httpClient.execute(httpPost);
                jsonResult = inputStreamToString(response.getEntity().getContent()).toString();
                System.out.println("Returned Json object " + jsonResult.toString());
                Log.v(TAG, "Got all Infos");

            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return jsonResult;
        }
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progressDialog = ProgressDialog.show(MainActivity.this, "Geo-Quiz Starting Up","Wait....", true);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.v(TAG, "Got all Infos Secondly");
            System.out.println("Resulted Value: " + result);
            Log.v(TAG, "Got all Infos Thirdly");
            progressDialog.dismiss();
            parsedObject = returnParsedJsonObject(result);
            Log.v(TAG, "Still Confused");
            System.out.println("Testing the water 07 " + parsedObject[0] + " - " + parsedObject[1]);
            System.out.println("Testing the water 08 " + parsedObject[2] + " - " + parsedObject[3]);
            if (parsedObject == null){
                return;
            }
            Log.v(TAG, "Still Confused again");
            Log.v(TAG, "I am finally back");
            GAME_DASHBOARD_HEADING01 = parsedObject[0] ;
            GAME_DASHBOARD_INFO01 = parsedObject[1] ;
            GAME_DASHBOARD_HEADING02 = parsedObject[2] ;
            GAME_DASHBOARD_INFO02 = parsedObject[3] ;

            if (session.checkLogin()) {
                //session.logoutUser();
                Intent i = new Intent(MainActivity.this, DashboardActivity.class);
                startActivity(i);
                MainActivity.this.finish();
            } else {
                Intent i = new Intent(MainActivity.this, WelcomeActivity.class);
                startActivity(i);
                MainActivity.this.finish();
            }

        }

        private StringBuilder inputStreamToString(InputStream is) {
            String rLine = "";
            StringBuilder answer = new StringBuilder();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            try {
                while ((rLine = br.readLine()) != null) {
                    answer.append(rLine);
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return answer;
        }
    }

    private String[] returnParsedJsonObject(String result){

        String[] jsonObject = new String[4];
        JSONObject resultObject = null;
        JSONArray jsonArray = null;
        String newItemObject = null;

        Log.v(TAG, "Got all the info -"+0);
        try {
            resultObject = new JSONObject(result);
            System.out.println("Testing the water " + resultObject.toString());
            Log.v(TAG, "Got all the info -" + 1);
            jsonArray = resultObject.optJSONArray("info_01");
            Log.v(TAG, "Got all the info -"+2);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.v(TAG, "Got all the info -"+3);
        Log.v(TAG, "Got all the info " + jsonArray.length());
        for(int i = 0; i < jsonArray.length(); i++){
            JSONObject jsonChildNode = null;
            try {
                jsonChildNode = jsonArray.getJSONObject(i);
                jsonObject[0] = jsonChildNode.getString("Heading01");
                jsonObject[1] = jsonChildNode.getString("Info01");

                jsonObject[2] = jsonChildNode.getString("Heading02");
                jsonObject[3] = jsonChildNode.getString("Info02");
                //newItemObject = new String(id, question, answerOptions, correctAnswer);
                //jsonObject.add(newItemObject);
                System.out.println("Testing the water 01 " + jsonObject[0] + " - "+ jsonObject[1] );
                System.out.println("Testing the water 02 " + jsonObject[2] + " - "+ jsonObject[3] );
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jsonObject;
    }

}
