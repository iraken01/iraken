package com.otaboroid.familytree;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.BitmapAjaxCallback;
import com.otaboroid.familytree.Objects.Question;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import com.androidquery.AQuery;
import com.otaboroid.familytree.service.QuizActivity;

/**
 * Created by koi10 on 20/11/2015.
 */
public class FullQuizActivity extends QuizActivity implements View.OnClickListener {

    private int MAX_QUESTION_CNT ;
    private int questionCount;
    private int correctCount = 0;
    private List<Question> questions = new ArrayList<Question>();
    private List<Question> parsedObject = new ArrayList<Question>();
    private int currentQuizQuestion;
    private int quizCount;
    private Question firstQuestion;

    private Button nextButton;
    private RadioGroup radioGroup;
    private RadioButton radioButtonA;
    private RadioButton radioButtonB;
    private RadioButton radioButtonC;
    private RadioButton radioButtonD;
    private String filename ;
    TextView questionTextSwitcher;
    ImageView questionImageSwitcher;
    public static final String TAG ="QZ_01" ;

    // AQuery object
    private AQuery aq;
    // Progress Dialog bar object
    private ProgressDialog prgDialog;

    private String Image_Url;

    int width ;
    int height ;
    private DisplayMetrics metrics;

    float smallestWidth ;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullquiz);

        //questions = selectQuestions();
        AsyncJsonObject asyncObject = new AsyncJsonObject();
        asyncObject.execute("");

        aq = new AQuery(this);

        Animation in = AnimationUtils.loadAnimation(this, android.R.anim.fade_in);
        Animation out = AnimationUtils.loadAnimation(this, android.R.anim.fade_out);
        nextButton = (Button)findViewById(R.id.next_button);
        radioGroup = (RadioGroup)findViewById(R.id.choices);
        radioButtonA = (RadioButton)findViewById(R.id.choice_a);
        radioButtonB = (RadioButton)findViewById(R.id.choice_b);
        radioButtonC = (RadioButton)findViewById(R.id.choice_c);
        radioButtonD = (RadioButton)findViewById(R.id.choice_d);
        // Set up Text Switcher
        questionTextSwitcher = (TextView) findViewById(R.id.question_label);
        // Set up Image Switcher
        questionImageSwitcher = (ImageView) findViewById(R.id.question_image);


        Display display = getWindowManager().getDefaultDisplay();
        width = display.getWidth();
        height = display.getHeight();

        Log.v("FullQuiz","Screen Size is "+Integer.toString(width));


        // device specific settings
        metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int widthPixels = metrics.widthPixels;
        int heightPixels = metrics.heightPixels;
        float scaleFactor = metrics.density;
        float widthDp = widthPixels / scaleFactor ;
        float heightDp = heightPixels / scaleFactor ;

        smallestWidth = Math.min(widthDp, heightDp);
        System.out.print("smallestWidth");
        System.out.print(smallestWidth);
        Log.i("FullQuiz", "smallestWidth" + Float.toString(smallestWidth));

        Log.v("FullQuiz", "Screen Size is " + Float.toString(smallestWidth));

        /*
        // Set up Text Switcher
        questionTextSwitcher.setInAnimation(in);
        questionTextSwitcher.setOutAnimation(out);
        questionTextSwitcher.setFactory(new MyTextSwitcherFactory());

        // Set up Image Switcher
        questionImageSwitcher.setInAnimation(in);
        questionImageSwitcher.setOutAnimation(out);
        questionImageSwitcher.setFactory(new MyImageSwitcherFactory());
        */

        nextButton.setOnClickListener(this);

        nextButton.setVisibility(View.INVISIBLE);

        // edit screen
        //editWidgetsOnScreen(0);
    }



    private void editWidgetsOnScreen(int index) {

        File ext = Environment.getExternalStorageDirectory();
        File target = new File(ext, "image/test.jpg");

        /* Settings of Image */
        //set the max number of concurrent network connections, default is 4
        AjaxCallback.setNetworkLimit(8);

        //set the max number of icons (image width <= 50) to be cached in memory, default is 20
        BitmapAjaxCallback.setIconCacheLimit(50);

        //set the max number of images (image width > 50) to be cached in memory, default is 20
        BitmapAjaxCallback.setCacheLimit(50);

        System.out.println("Editing screen widget for question " + index);
        System.out.println("Getting Question Object");

        Question question = questions.get(index);

        System.out.println("Getting Question Image url");
        Image_Url = question.getmyPicture();
        System.out.println("Done with number" + index + Image_Url);
        prgDialog = new ProgressDialog(this);
        prgDialog.setMessage("Downloading Image from Internet. Please wait...");
        prgDialog.setIndeterminate(false);
        prgDialog.setMax(100);
        prgDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        prgDialog.setCancelable(false);
        // Image loading with Progress Dialog Bar
        /*
        aq.id(R.id.next_button).text("Load Image with progress dialog bar").clicked(this, "loadImageFromInternetWithProgressDialogBar");
        prgDialog = new ProgressDialog(this);
        prgDialog.setMessage("Downloading Image from Internet. Please wait...");
        prgDialog.setIndeterminate(false);
        prgDialog.setMax(100);
        prgDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        prgDialog.setCancelable(false);
        Toast.makeText(aq.getContext(), "Download initiated...", Toast.LENGTH_SHORT).show();
        // Now populate the screen
        //qimage = getmyPicture(question.getQuestionNum());
        */

        if(Image_Url.equals("none")){
            questionImageSwitcher.setVisibility(View.GONE);
        }else {
            //questionImageSwitcher.setImageDrawable(qimage);
            //aq.id(R.id.question_image).progress(prgDialog).image(Image_Url, true, true, 300, R.drawable.iraken_icon);
            //aq.getCachedImage(Image_Url) ;
            //aq.id(R.id.question_image).getCachedImage(Image_Url) ;
            /* Getting Images from Server and stored in cache */
            //aq.id(R.id.question_image).progress(prgDialog).image(Image_Url, true, true, 0, R.drawable.iraken_icon, null, AQuery.FADE_IN);
            //aq.id(R.id.question_image).progress(prgDialog).image(target,300);
            //Bitmap preset = aq.getCachedImage(Image_Url, 300);
            Bitmap preset = aq.getCachedImage(Image_Url, (int) smallestWidth);
            aq.id(R.id.question_image).image(preset);
            questionImageSwitcher.setVisibility(View.VISIBLE);
        }
        questionTextSwitcher.setText(question.getQuestion());
        radioButtonA.setText(question.getChoiceA());
        radioButtonB.setText(question.getChoiceB());
        radioButtonC.setText(question.getChoiceC());
        radioButtonD.setText(question.getChoiceD());

        radioButtonA.setTextColor(getResources().getColor(R.color.full_quiz_text_color));
        radioButtonB.setTextColor(getResources().getColor(R.color.full_quiz_text_color));
        radioButtonC.setTextColor(getResources().getColor(R.color.full_quiz_text_color));
        radioButtonD.setTextColor(getResources().getColor(R.color.full_quiz_text_color));
        /*
        radioButtonA.setTextColor(Color.BLACK);
        radioButtonB.setTextColor(Color.BLACK);
        radioButtonC.setTextColor(Color.BLACK);
        radioButtonD.setTextColor(Color.BLACK);
        */
        // Update Question XML file and comare strings
    }

    /*
    */

    @Override
    public void onClick(View v) {
        questionCount++;
        if (questionCount == MAX_QUESTION_CNT) {
            Intent intent = new Intent(FullQuizActivity.this, ResultActivity.class);
            intent.putExtra("correctAnswerCount", correctCount);
            intent.putExtra("NumerOfQuestions", MAX_QUESTION_CNT);

            startActivity(intent);
            System.exit(0);
        } else {
            if (questionCount == MAX_QUESTION_CNT - 1) {
                System.out.println("qmax again " + GAME_MAX_QUESTION_CNT);
                //   nextButton.setText(R.string.done_button);
            }
            editWidgetsOnScreen(questionCount);
            nextButton.setVisibility(View.INVISIBLE);
            radioButtonA.setEnabled(true);
            radioButtonB.setEnabled(true);
            radioButtonC.setEnabled(true);
            radioButtonD.setEnabled(true);
            radioGroup.clearCheck();
        }
    }

    public void onRadioButtonClicked(View view) {
        RadioButton checkedRadio = (RadioButton)view;
        int correctAnswer = questions.get(questionCount).getAnswer();
        checkedRadio.setTextColor(Color.RED);
        RadioButton correctRadio = (RadioButton)radioGroup.getChildAt(correctAnswer-1);
        correctRadio.setTextColor(Color.GREEN);
        int correctRadioId = correctRadio.getId();
        if (checkedRadio.getId() == correctRadioId) {
            correctCount++;
        }
        nextButton.setVisibility(View.VISIBLE);
        radioButtonA.setEnabled(false);
        radioButtonB.setEnabled(false);
        radioButtonC.setEnabled(false);
        radioButtonD.setEnabled(false);

    }

    /**
     * A switcher factory for use with the question image.
     * Creates the next {@code ImageView} object to animate to
     *
     */
    private class MyImageSwitcherFactory implements ViewSwitcher.ViewFactory {
        public View makeView() {
            ImageView imageView = new ImageView(FullQuizActivity.this);
            imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            imageView.setLayoutParams(new ImageSwitcher.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT));
            return imageView;
        }
    }

    /**
     * A switcher factory for use with the question text.
     * Creates the next {@code TextView} object to animate to
     *
     */
    private class MyTextSwitcherFactory implements ViewSwitcher.ViewFactory {
        public View makeView() {
            TextView textView = new TextView(FullQuizActivity.this);
            textView.setGravity(Gravity.CENTER);
            Resources res = getResources();
            float dimension = res.getDimension(R.dimen.game_question_size);
            int titleColor = res.getColor(R.color.title_color);
            int shadowColor = res.getColor(R.color.title_glow);
            textView.setTextSize(dimension);
            textView.setTextColor(titleColor);
            textView.setShadowLayer(10, 5, 5, shadowColor);
            return textView;
        }
    }


    private int randomWithRange(int min, int max)
    {
        int range = (max - min) + 1;
        return (int)(Math.random() * range) + min;
    }


    private class AsyncJsonObject extends AsyncTask<String, Void, String> {

        private ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {

            Log.v(TAG, "Ready to download");
            HttpClient httpClient = new DefaultHttpClient(new BasicHttpParams());
            HttpPost httpPost = new HttpPost("http://akdigitzed.gear.host/geo_quiz_api/data_03/index01.php");
            String jsonResult = "";

            try {
                HttpResponse response = httpClient.execute(httpPost);
                jsonResult = inputStreamToString(response.getEntity().getContent()).toString();
                System.out.println("Returned Json object " + jsonResult.toString());
                Log.v(TAG, "Got all questions");

            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return jsonResult;
        }
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            progressDialog = ProgressDialog.show(FullQuizActivity.this, "Downloading Quiz","Wait....", true);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();
            System.out.println("Resulted Value: " + result);
            returnParsedJsonObject(result);
            //parsedObject = returnParsedJsonObject(result);
            /*
            returnParsedJsonObject(result);
            quizQuestion.setText(firstQuestion.getQuestion());
            String[] possibleAnswers = firstQuestion.getAnswers().split(",");
            optionOne.setText(possibleAnswers[0]);
            optionTwo.setText(possibleAnswers[1]);
            optionThree.setText(possibleAnswers[2]);
            optionFour.setText(possibleAnswers[3]);
            */
        }

        private StringBuilder inputStreamToString(InputStream is) {
            String rLine = "";
            StringBuilder answer = new StringBuilder();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            try {
                while ((rLine = br.readLine()) != null) {
                    answer.append(rLine);
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return answer;
        }
    }


    private void returnParsedJsonObject(String result){

        List<Question> jsonObject = new ArrayList<Question>();
        JSONObject resultObject = null;
        JSONArray jsonArray = null;
        Question newItemObject = new Question();

        Log.v(TAG, "Got many questions-"+0);
        try {
            resultObject = new JSONObject(result);
            System.out.println("Testing the water " + resultObject.toString());
            Log.v(TAG, "Got all questions-" + 1);
            jsonArray = resultObject.optJSONArray("geophysics01");
            Log.v(TAG, "Got all questions-"+2);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.v(TAG, "Got all questions-"+3);
        Log.v(TAG, "Got all questions"+jsonArray.length());

        MAX_QUESTION_CNT = jsonArray.length() ;
        GAME_MAX_QUESTION_CNT = MAX_QUESTION_CNT ;
        try {
            resultObject = new JSONObject(result);
            System.out.println("Testing the water " + resultObject.toString());
            Log.v(TAG, "Got all questions-" + 1);
            jsonArray = resultObject.optJSONArray("geophysics01");
        } catch (JSONException e) {
            e.printStackTrace();
        }


        for(int i = 0; i < jsonArray.length(); i++){
            JSONObject jsonChildNode = null;
            String myPix ;
            File ext = Environment.getExternalStorageDirectory();
            File target = new File(ext, "image/test.jpg");

            try {
                jsonChildNode = jsonArray.getJSONObject(i);
                int id = jsonChildNode.getInt("id");
                String question = jsonChildNode.getString("question");
                String A = jsonChildNode.getString("option_a") ;
                String B = jsonChildNode.getString("option_b") ;
                String C = jsonChildNode.getString("option_c") ;
                String D = jsonChildNode.getString("option_d") ;
                int myAnswer = jsonChildNode.getInt("correct_option");
                myPix = jsonChildNode.getString("image_url") ;

                // cache image now if need be
                Log.v("FullQuiz",myPix);
                if (myPix.equals("none") ){
                }else {
                    //aq.cache(myPix,0).getCachedImage(myPix,300) ;
                    aq.cache(myPix, 0);

                    Log.v("FullQuiz", "Image Cached");
                    /*
                    aq.download(myPix, target, new AjaxCallback<File>() {
                        public void callback(String url, File file, AjaxStatus status) {
                            if (file != null) {
                                //Toast.makeText(getApplicationContext(), "File:" + file.length() + ":" + file + "-" + status,
                                //       Toast.LENGTH_LONG).show();
                                Log.v("FullQuiz", "Download was succesful");
                            } else {
                                Log.v("FullQuiz", "Download failed");
                                //Toast.makeText(getApplicationContext(), "Download Failed", Toast.LENGTH_LONG).show();
                            }
                        }

                    });
                    */
                }
                newItemObject = new Question(id, question, A, B,  C,  D, myAnswer, myPix);
                jsonObject.add(newItemObject);

                System.out.println("Question " + i + " : " + newItemObject.getQuestion());
                System.out.println("Question " + i + " : " + jsonChildNode.getString("question"));
                System.out.println("Done with question " + i);
                System.out.println("Question Number One -  " + jsonObject.get(i).getQuestion());
                questions.add(i, jsonObject.get(i));


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        /*
        Log.v(TAG, "Got all questions-"+5567);
        System.out.println("Question Number One -  " + jsonObject.get(0).getQuestion());
        System.out.println("Question Number Two -  " + jsonObject.get(1).getQuestion());
        System.out.println("Question Number Three -  " + jsonObject.get(2).getQuestion());
        System.out.println("Question Number Four -  " + jsonObject.get(3).getQuestion());
        */
        //questions = parsedObject ;
        if(questions == null){
            return;
        }
        quizCount = questions.size();
        // edit screen
        //questions = parsedObject ;
        for(int i = 0; i < jsonArray.length(); i++) {
            System.out.println("Question Number One -  " + questions.get(i).getQuestion());
        }
        GAME_MAX_QUESTION_CNT = jsonArray.length();
        System.out.println("qmax  " + GAME_MAX_QUESTION_CNT);
        editWidgetsOnScreen(0);
        //return jsonObject;
    }
}