package com.otaboroid.familytree.service;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.otaboroid.familytree.DashboardActivity;
import com.otaboroid.familytree.FreeQuizActivity;
import com.otaboroid.familytree.FullQuizActivity;
import com.otaboroid.familytree.R;

/**
 * Created by koi10 on 18/11/2015.
 */
public class GridViewAdapter extends BaseAdapter {

    String [] result;
    Context context;
    int [] imageId;
    private static LayoutInflater inflater=null;
    public GridViewAdapter (DashboardActivity mainActivity, String[] prgmNameList, int[] prgmImages) {
        // TODO Auto-generated constructor stub
        result=prgmNameList;
        context=mainActivity;
        imageId=prgmImages;
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return result.length;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder
    {
        TextView tv;
        ImageView img;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Holder holder=new Holder();
        View rowView;

        rowView = inflater.inflate(R.layout.grid_item, null);
        holder.tv=(TextView) rowView.findViewById(R.id.gridtext);
        holder.img=(ImageView) rowView.findViewById(R.id.gridimage);

        holder.tv.setText(result[position]);
        holder.img.setImageResource(imageId[position]);


        rowView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                int ImageID ;
                // TODO Auto-generated method stub
                //Toast.makeText(context, "You Clicked "+result[position], Toast.LENGTH_SHORT).show();


                if (position == 4) {
                    Intent intent = new Intent(context, FreeQuizActivity.class);
                    context.startActivity(intent);
                    System.exit(0);
                }

                if (position == 1) {
                    Intent intent = new Intent(context, FullQuizActivity.class);
                    context.startActivity(intent);
                    System.exit(0);
                }

                /*
                //Create intent

                if (position == 5) {
                    Intent intent = new Intent(context, CorporateActivity.class);
                    context.startActivity(intent);
                    //System.exit(0);
                } else if (position == 2) {
                    Intent intent = new Intent(context, BooksActivity.class);
                    context.startActivity(intent);
                    //System.exit(0);
                } else if (position == 1) {
                    Intent intent = new Intent(context, ClassesActivity.class);
                    context.startActivity(intent);
                    //System.exit(0);
                } else if (position == 0) {
                    Intent intent = new Intent(context, ProfileActivity.class);
                    context.startActivity(intent);
                    //System.exit(0);
                } else if (position == 3) {
                    Intent intent = new Intent(context, TutorialActivity.class);
                    context.startActivity(intent);
                    //System.exit(0);
                } else if (position == 4) {
                    Intent intent = new Intent(context, QuizActivity.class);
                    context.startActivity(intent);
                    //System.exit(0);
                } else {
                    Intent intent = new Intent(context, CorporateActivity.class);
                    ImageID = imageId[position];
                    intent.putExtra("title", result[position]);
                    intent.putExtra("imageId", ImageID);
                    //Start details activity
                    context.startActivity(intent);

                }
                */
            }

        });

        return rowView;
    }

}
