package com.otaboroid.familytree.service;

import android.app.Activity;
import android.support.v7.app.ActionBarActivity;

/**
 * Created by koi10 on 19/11/2015.
 */
public class QuizActivity extends ActionBarActivity {

    // Game preference values
    public static final String GAME_PREFERENCES = "GamePrefs";
    public static final String GAME_PREFERENCES_NICKNAME = "Nickname"; // String
    public static final String GAME_PREFERENCES_EMAIL = "Email"; // String
    public static final String GAME_PREFERENCES_PASSWORD = "Password"; // String
    public static final String GAME_PREFERENCES_DOB = "DOB"; // Long
    public static final String GAME_PREFERENCES_GENDER = "Gender"; // Integer, in array order: Male (1), Female (2), and Undisclosed (0)
    public static final String GAME_PREFERENCES_SCORE = "Score"; // Integer
    public static final String GAME_PREFERENCES_CURRENT_QUESTION = "CurQuestion"; // Integer
    public static final String GAME_PREFERENCES_AVATAR = "Avatar"; // String URL to image

    public static final String GAME_PREFERENCES_FAV_PLACE_NAME = "FavPlaceName"; // String
    public static final String GAME_PREFERENCES_FAV_PLACE_LONG = "FavPlaceLong"; // float
    public static final String GAME_PREFERENCES_FAV_PLACE_LAT = "FavPlaceLat"; // float

    public static final String GAME_PREFERENCES_PLAYER_ID = "ServerId"; // Integer

    // XML Tag Names
    public static final String XML_TAG_QUESTION_BLOCK = "questions";
    public static final String XML_TAG_QUESTION = "question";
    public static final String XML_TAG_QUESTION_ATTRIBUTE_NUMBER = "number";
    public static final String XML_TAG_QUESTION_ATTRIBUTE_TEXT = "text";
    public static final String XML_TAG_QUESTION_ATTRIBUTE_CORRECT_OPTION = "answer";
    public static final String XML_TAG_QUESTION_ATTRIBUTE_CHOICES = "choices";
    public static final String XML_TAG_QUESTION_ATTRIBUTE_IMAGEURL = "imageUrl";
    public static final int QUESTION_BATCH_SIZE = 25; // 15;

    // Server URLs
    public static final String TRIVIA_SERVER_BASE = "\"http://akdigitzed.gear.host/";
    public static final String TRIVIA_SERVER_SCORES = TRIVIA_SERVER_BASE + "naija_quiz/index01.php";
    public static final String TRIVIA_SERVER_QUESTIONS = TRIVIA_SERVER_BASE + "naija_quiz/index01.php";
    public static final String TRIVIA_SERVER_FRIEND_EDIT = TRIVIA_SERVER_BASE + "friend";
    public static final String TRIVIA_SERVER_ACCOUNT_EDIT = TRIVIA_SERVER_BASE + "receive";

    public static final String DEBUG_TAG = "Trivia Quiz Log";

    public static String GAME_DASHBOARD_HEADING01 = "000000"; // Integer
    public static String GAME_DASHBOARD_INFO01 = "000000"; // Integer
    public static String GAME_DASHBOARD_HEADING02 = "000000"; // Integer
    public static String GAME_DASHBOARD_INFO02 = "000000"; // Integer

    public int GAME_MAX_QUESTION_CNT ;
}
