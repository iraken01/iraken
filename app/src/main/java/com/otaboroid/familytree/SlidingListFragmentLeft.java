package com.otaboroid.familytree;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import io.github.kexanie.library.MathView;

/**
 * Created by kennethirabor on 07/11/2016.
 */

//public class SlidingListFragmentLeft extends ListFragment {
public class SlidingListFragmentLeft extends Fragment {

    private MathView Mquestions;
    private QuizComp2Activity activity ;


    @Override
    public void onAttach (Activity activity) {
        this.activity = (QuizComp2Activity) activity;
        super.onAttach(activity);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //return inflater.inflate(R.layout.sliding_fragment_layout_left, container, false);
        return inflater.inflate(R.layout.fragment_quizcomp, container, false);
        //fragment_quiz
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        Animation fade2 = AnimationUtils.loadAnimation(activity, R.anim.fade_in2);
        // get question webview
        Mquestions = (MathView) view.findViewById(R.id.frag1_quest_Q1);


        //Mquestions.startAnimation(fade2);


        fade2.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                /**
                 * Hopefully the equation would have rendered properly now
                 * Time to show the Questions
                 * */
                Mquestions.setVisibility(View.VISIBLE);
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        });



    }

    /*
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setListAdapter(new MyListAdapter());
    }

    private class MyListAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return 30;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            TextView result = (TextView) convertView;
            if (result == null) {
                result = (TextView) LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.list_view_item, parent, false);
            }
            result.setText("My custom element #" + position);

            return result;
        }
    }
    */
}
