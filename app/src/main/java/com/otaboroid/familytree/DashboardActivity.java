package com.otaboroid.familytree;

import android.app.ProgressDialog;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.otaboroid.familytree.service.GridViewAdapter;
import com.otaboroid.familytree.service.ImageItem;
import com.otaboroid.familytree.service.QuizActivity;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by koi10 on 18/11/2015.
 */
public class DashboardActivity extends QuizActivity {

    private GridView gridView;
    private ArrayList<ImageItem> Data1 ;
    private TextView infText1;
    private TextView infText2;
    private TextView infText3;
    private TextView infText4;
    private GridViewAdapter gridAdapter;
    private ViewFlipper flipper;
    private String display1 ;
    private String display2 ;

    private static String [] prgmNameList ;
    private static int [] prgmImages ;

    private String[] parsedObject= new String[4] ;
    //String[] jsonObject = new String[4];
    public static final String TAG ="QZ_01" ;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        /*
        * Configure ViewFlipper
        * */


        flipper = (ViewFlipper) findViewById(R.id.viewFlipper1);
        flipper.setInAnimation(getApplicationContext(), R.anim.in_from_left);
        flipper.setOutAnimation(getApplicationContext(), R.anim.prop_exit);
        flipper.setFlipInterval(4000);
        flipper.startFlipping();
        flipper.setFocusable(false);


        /*
        * Done with ViewFlipper
        * */

        //marqText = (TextView) findViewById(R.id.marqueetextdb);
        /*
        display1 = "IDEAS || I was you could use the place to reach and share you thoughts. But then ";
        display1 = display1 + " we can always take it out to get more space here. ||   ";
        display1 = display1 + "INFO || This is an Alpha Version. The Beta Version I was going to send ";
        display1 = display1 + " last night seems to have some bugs in it, undergoing FIX!  || ";

        //marqText = (TextView) findViewById(R.id.marqueetextdb);
        display2 = "IDEA ||  The idea is to use this means to broadcast info to your students. We can  " ;
        display2 = display2 +  "Also move its position, into the 'My Classes' area ||   " ;
        display2 = display2 +  "ALERT!!! Yaba Tech Students taking my Course in Financial Accounting (FA)  " ;
        display2 = display2 + " should be aware that I have MOVED ";
        display2 = display2 + " the DATES of the Assessment Test INDEFINITELY, due to recent happenings ";
        display2 = display2 + " on Campus !!! ";
        display2 = display2 + " More details to follow later ";

        display2 = display2 + " ........                   ........  ";
        display2 = display2 + " INFO ! ";
        display2 = display2 + " My Cost Accounting Students are hereby encouraged to take my  ";
        display2 = display2 + " Cost Accounting Quiz on this App. It will help rate your level  ";
        display2 = display2 + " of understanding of the materials we have covered in Class.  ";
        */
        display1 = GAME_DASHBOARD_INFO01 ;
        display2 = GAME_DASHBOARD_INFO02 ;

        infText3 = (TextView) findViewById(R.id.mesg_talks);
        infText4 = (TextView) findViewById(R.id.mesg_alert);
        infText3.setText(GAME_DASHBOARD_HEADING01);
        infText4.setText(GAME_DASHBOARD_HEADING02);


        infText1 = (TextView) findViewById(R.id.infotextdb1);
        infText1.setText(display1);
        //infText1.setText(parsedObject[1]);
        infText1.setHorizontallyScrolling(true);

        infText2 = (TextView) findViewById(R.id.infotextdb2);
        infText2.setText(display2);
        //infText2.setText(parsedObject[3]);
        infText2.setHorizontallyScrolling(true);
        infText2.setSelected(true);  // Set focus to the textview
        infText2.setFocusable(true) ;

        Data1=getData() ;
        prgmNameList = getIconNames() ;
        prgmImages = getIconIds() ;

        gridView=(GridView) findViewById(R.id.gridView01);
        GridViewAdapter gridAdapter = new GridViewAdapter(DashboardActivity.this, prgmNameList, prgmImages);
        gridView.setAdapter(gridAdapter);

        flipper.setOnTouchListener(new View.OnTouchListener() {
            float x1 = 0, x2, y1 = 0, y2, dx, dy;

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                String direction;
                switch (event.getAction()) {
                    case (MotionEvent.ACTION_DOWN):
                        x1 = event.getX();
                        y1 = event.getY();
                        break;
                    case (MotionEvent.ACTION_UP): {
                        x2 = event.getX();
                        y2 = event.getY();
                        dx = x2 - x1;
                        dy = y2 - y1;
                        // Use dx and dy to determine the direction
                        if (Math.abs(dx) > Math.abs(dy)) {
                            if (dx > 0) {
                                direction = "right";
                            } else {
                                direction = "left";
                            }
                            slideFipper(direction);
                        } else {
                            if (dy > 0) direction = "down";
                            else direction = "up";
                        }
                    }
                }
                v.performClick();
                return true;
            }
        });

    }




    // Prepare some dummy data for gridview
    private ArrayList<ImageItem> getData () {
        final ArrayList<ImageItem> imageItems = new ArrayList<>();
        TypedArray imgs = getResources().obtainTypedArray(R.array.image_ids);
        for (int i = 0; i < imgs.length(); i++) {
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), imgs.getResourceId(i, -1));
            String imageNam = getIconNam(i);
            imageItems.add(new ImageItem(bitmap, imageNam));
        }
        return imageItems;
    }


    // Get icon name
    private String[] getIconNames () {
        String [] Full_names = new String[6];
        for (int k = 0; k < 6; k++) {
            Full_names[k] = getIconNam (k);
        }
        return Full_names ;
    }

    // Get icon ids
    private int [] getIconIds () {
        int [] Full_Ids = new int[6];
        TypedArray imgs = getResources().obtainTypedArray(R.array.image_ids);
        for (int k = 0; k < 6; k++) {
            Full_Ids[k] = imgs.getResourceId(k, -1) ;

        }
        return Full_Ids ;
    }

    // Get icon name
    private String getIconNam (int i) {
        String ImageNam ;

        if (i == 0) {
            ImageNam = "My Profile";
        } else if (i == 1) {
            ImageNam = "My Classes";
        } else if (i == 2) {
            ImageNam = "My Books";
        } else if (i == 3) {
            ImageNam = "Quick Tutorial";
        } else if (i == 4) {
            ImageNam = "My Quiz";
        } else if (i == 5) {
            ImageNam = "My Services";
        } else
        {
            ImageNam = " " ;
        }

        return ImageNam ;
    }




    private static Handler handler = new Handler();

    public void slideFipper(final String direction){
        final Animation no_next = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.no_next);
        final Animation no_previuos = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.no_previous);
        handler.post(new Runnable(){
            public void run(){
                int currentView = flipper.getDisplayedChild();
                if (direction.equalsIgnoreCase("left")) {
                    //left
                    if (currentView < 2) {
                        //flipper.setInAnimation(getApplicationContext(), R.anim.enter);
                        flipper.setInAnimation(getApplicationContext(), R.anim.in_from_left);
                        //flipper.setInAnimation(getApplicationContext(), R.anim.fade_in);
                        flipper.setOutAnimation(getApplicationContext(), R.anim.exit);
                        flipper.showNext();
                    } else {
                        flipper.startAnimation(no_next);
                    }
                } else {
                    //right
                    if (currentView > 0) {
                        //flipper.setInAnimation(getApplicationContext(), R.anim.fade_in2);
                        flipper.setInAnimation(getApplicationContext(), R.anim.in_from_right);
                        //flipper.setInAnimation(getApplicationContext(),R.anim.prop_enter);
                        flipper.setOutAnimation(getApplicationContext(), R.anim.prop_exit);
                        flipper.showPrevious();
                    } else {
                        flipper.startAnimation(no_previuos);
                    }
                }
            }
        });
    }



}

