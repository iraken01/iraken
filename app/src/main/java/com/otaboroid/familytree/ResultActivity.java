package com.otaboroid.familytree;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.otaboroid.familytree.service.QuizActivity;

import java.util.Locale;

/**
 * Created by koi10 on 20/11/2015.
 */
public class ResultActivity extends QuizActivity implements View.OnClickListener {

    private Button doneButton;
    //private Button doAgainButton;
    private Button doAgainButton;
    private TextView messageTextView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.result_layout);
        messageTextView = (TextView)findViewById(R.id.message_label);
        doneButton = (Button)findViewById(R.id.finish_button);
        doAgainButton = (Button)findViewById(R.id.do_again_button);
        doneButton.setOnClickListener(this);
        doAgainButton.setOnClickListener(this);
        doAgainButton.setVisibility(View.INVISIBLE);
        int correctAnswerCount = this.getIntent().getIntExtra("correctAnswerCount", 0);
        int numOfQuestions = this.getIntent().getIntExtra("NumerOfQuestions", 0);
        //Score 0/5, 1/5 and 2/5 Message => "Please try again!"
        //Score 3/5 Message => "Good job!"
        ///Score 4/5 Message => "Excellent work!"
        //Score 5/5 Message => "You are a genius!"
        String message = "";
        System.out.println("qmax  " + GAME_MAX_QUESTION_CNT);
        //message = String.format(Locale.US, "You have scored %d, \n out of %d", correctAnswerCount, GAME_MAX_QUESTION_CNT);
        message = String.format(Locale.US, "You have scored %d, \n out of %d", correctAnswerCount, numOfQuestions);
        doAgainButton.setVisibility(View.VISIBLE);
        messageTextView.setText(message);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.finish_button:
                System.exit(0);
                break;
            case R.id.do_again_button:
                Intent intent = new Intent(ResultActivity.this, DashboardActivity.class);
                startActivity(intent);
                System.exit(0);
                break;
        }
    }


}