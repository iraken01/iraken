package com.otaboroid.familytree.Objects;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.otaboroid.familytree.R;

/**
 * Created by koi10 on 20/11/2015.
 */
public class Picture extends ImageView {
    public Picture(Context context) {
        super(context);
    }
    public Picture(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    public Picture(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
    public void drawPic() {
        setImageResource(R.drawable.iraken_icon);
    }
}

/**
 @Override
 public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
 View view = inflater.inflate(R.layout.main_layout, container, false);
 RelativeLayout layout= (RelativeLayout) view.findViewById(R.id.question_image_layout);
 ImageView picture = new ImageView(this);
 picture.setImageResource(R.drawable.splash1);
 layout.addView(picture);
 return view;
 }

 */