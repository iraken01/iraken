package com.otaboroid.familytree.Objects;

/**
 * Created by koi10 on 20/11/2015.
 */
public class Question{

    private String question;
    private String choiceA;
    private String choiceB;
    private String choiceC;
    private String choiceD;
    private String myPicture;
    private int answer;
    private int questnum;

    public Question() {
        super();
    }

    public Question(int id, String question, String A, String B, String C, String D, int myAnswer, String myPix) {
        this.questnum = id;
        this.question = question;
        this.choiceA = A;
        this.choiceB = B;
        this.choiceC = C;
        this.choiceD = D;
        this.answer = myAnswer;
        this.myPicture = myPix;
    }

    @Override
    public String toString() {
        return "Question [question=" + question
                + ", choiceA=" + choiceA
                + ", choiceB=" + choiceB
                + ", choiceC=" + choiceC
                + ", choiceD=" + choiceD
                + ", picture=" + myPicture
                + ", answer=" + answer + "]";
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getChoiceA() {
        return choiceA;
    }

    public void setChoiceA(String choiceA) {
        this.choiceA = choiceA;
    }

    public String getChoiceB() {
        return choiceB;
    }

    public void setChoiceB(String choiceB) {
        this.choiceB = choiceB;
    }

    public String getChoiceC() {
        return choiceC;
    }

    public void setChoiceC(String choiceC) {
        this.choiceC = choiceC;
    }

    public String getChoiceD() {
        return choiceD;
    }

    public void setChoiceD(String choiceD) {
        this.choiceD = choiceD;
    }

    public void setmyPicture(String myPicture) {
        this.myPicture = myPicture;
    }

    public String getmyPicture() {return myPicture;}

    public int getAnswer() {
        return answer;
    }

    public void setAnswer(int answer) {
        this.answer = answer;
    }

    public int getQuestionNum() {
        return questnum;
    }

    public void setQuestionNum(int answer) {
        this.questnum = answer;
    }

}
